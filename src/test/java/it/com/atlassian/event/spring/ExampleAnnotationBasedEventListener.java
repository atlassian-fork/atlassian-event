package it.com.atlassian.event.spring;

import com.atlassian.event.api.EventListener;

/**
 * Example event listener that uses the @EventListener annotation.
 */
public class ExampleAnnotationBasedEventListener {
    public ExampleEvent event;

    @EventListener
    public void onEvent(ExampleEvent event) {
        this.event = event;
    }

    public void reset() {
        this.event = null;
    }

}
