package com.atlassian.event.internal;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.spi.ListenerInvoker;
import com.atlassian.plugin.eventlistener.descriptors.EventListenerModuleDescriptor;
import com.atlassian.plugin.scope.ScopeManager;
import org.slf4j.Logger;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.function.ToIntFunction;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkNotNull;

class EventPublisherUtils {
    private static final String PROPERTY_PREFIX = EventPublisherImpl.class.getName();
    private static final Optional<String> debugRegistration =
            Optional.ofNullable(System.getProperty(PROPERTY_PREFIX + ".debugRegistration"));
    private static final boolean debugRegistrationLocation =
            Boolean.getBoolean(PROPERTY_PREFIX + ".debugRegistrationLocation");
    private static final Optional<String> debugInvocation =
            Optional.ofNullable(System.getProperty(PROPERTY_PREFIX + ".debugInvocation"));
    private static final boolean debugInvocationLocation =
            Boolean.getBoolean(PROPERTY_PREFIX + ".debugInvocationLocation");
    
    static Set<ListenerInvokerWithClassHierarchyAndRegisterOrder> getInvokersWithClassHierarchyOrder(
            final Object event,
            final Function<Class<?>, Collection<ListenerInvokerWithRegisterOrder>> eventToListeners) {
        final Set<ListenerInvokerWithClassHierarchyAndRegisterOrder> invokers = new HashSet<>();
        final AtomicInteger classHierarchyOrder = new AtomicInteger();
        for (final Class<?> eventClass : ClassUtils.findAllTypes(checkNotNull(event).getClass())) {
            invokers.addAll(eventToListeners.apply(eventClass).stream()
                    .map(invoker -> new ListenerInvokerWithClassHierarchyAndRegisterOrder(invoker, classHierarchyOrder.get()))
                    .collect(Collectors.toList()));
            classHierarchyOrder.incrementAndGet();
        }
        return invokers;
    }

    /**
     * Use following properties for sorting:
     * <ol>
     *     <li>{@link EventListener#order()} </li>
     *     <li>event class hierarchy (from bottom to top)</li>
     *     <li>event listeners registering order</li>
     * </ol>
     */
    static Set<ListenerInvoker> sortInvokers(final ScopeManager scopeManager, final Set<ListenerInvokerWithClassHierarchyAndRegisterOrder> invokers) {
        final Comparator<ListenerInvokerWithClassHierarchyAndRegisterOrder> byDeclaredOrderThenClassHierarychOrderThenRegisterOrder =
                Comparator
                        .comparingInt((ToIntFunction<ListenerInvokerWithClassHierarchyAndRegisterOrder>) value -> value.getListenerInvokerWithRegisterOrder().getOrder())
                        .thenComparingInt(value -> value.classHierarchyOrder)
                        .thenComparingInt(value -> value.getListenerInvokerWithRegisterOrder().getRegisterOrder());

        return invokers.stream()
                .filter(i -> i.keyedListenerInvoker.getScope()
                        .map(scopeManager::isScopeActive)
                        .orElse(true))
                .sorted(byDeclaredOrderThenClassHierarychOrderThenRegisterOrder)
                .map(ListenerInvokerWithClassHierarchyAndRegisterOrder::getListenerInvokerWithRegisterOrder)
                .map(ListenerInvokerWithRegisterOrder::getInvoker)
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    static Object getListener(final Object listener) {
        if (listener instanceof EventListenerModuleDescriptor) {
            final EventListenerModuleDescriptor descriptor = (EventListenerModuleDescriptor) listener;
            return descriptor.getModule();
        } else {
            return listener;
        }
    }

    static boolean shouldDebugThisInvocation(final Object event) {
        final String eventClassName = event.getClass().getName();
        return debugInvocation.map(eventClassName::startsWith).orElse(false);
    }

    static void logInvocation(final Logger log, final Object event, final ListenerInvoker invoker) {
        log.warn("Listener invoked event with class '{}' -> invoker {}", event.getClass().getName(), invoker);
        if (EventPublisherUtils.debugInvocationLocation) {
            log.warn("Invoked from", new Exception());
        }
    }

    static void logRegistration(final Logger log, final Class<?> eventClass, final ListenerInvoker invoker) {
        debugRegistration.ifPresent(classPrefix -> {
            if (eventClass.getName().startsWith(classPrefix)) {
                log.warn("Listener registered event '{}' -> invoker {}", eventClass, invoker);
                if (debugRegistrationLocation) {
                    log.warn("Registered from", new Exception());
                }
            }
        });
    }
}
